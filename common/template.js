Vue.component('common-modal', {
	template: `<div id="gender_interest_modal" :class='showBox? "": "hidden"'>
	<div class="relative" :class='showBox? "": "hidden"'>
	  <div
		class="fixed top-0 left-0 right-0 bottom-0 z-50 bg-black bg-opacity-50 backdrop-blur-sm  email-modal modal-backdrop feedback-modal">
	  </div>
	  <div class="fixed inset-0 z-50 overflow-y-auto">
		<div class="items-end justify-center p-4 flex items-center min-h-screen text-center sm:items-center sm:p-0">
		  <div style="width: 322px;height: 296px;"
			class="relative bg-zinc-900 rounded-[10px] transform overflow-hidden text-left transition-all sm:my-8">
			<div class="absolute right-0 top-0 pr-4 pt-4 sm:block cursor-pointer">
			  <button class="rounded-md text-white focus:outline-none" @click='closeBox'>
				<span class="sr-only">Close</span>
				<svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
				  aria-hidden="true">
				  <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12"></path>
				</svg>
			  </button>
			</div>
			<div style="left: 40px;top: 40px;"
			  class="left-[40px] top-[40px] absolute flex-col justify-start items-center gap-6 inline-flex">
			  <div class="text-white text-xl font-bold font-[&#39;Poppins&#39;] leading-[34px]">I’m Interested in:
			  </div>
			  <div class="justify-start items-start gap-2.5 inline-flex">
				<button type="button" v-for='(item, index) in sexList' :style='{"background": index === sexIndex? "#494242": ""}' :key='index' @click='sexIndex = index'
				  class="gender-btn w-[116px] px-10 pt-4 pb-2.5 bg-neutral-800 rounded-[10px] border border-neutral-700 flex-col justify-end items-center gap-2 inline-flex">
				  <div class="w-9 h-9 relative flex-col justify-start items-start flex" v-if='item.title === "Male"'>
					<div class="w-[30px] h-[30px] relative">
					  <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36" fill="none">
						<path
						  d="M14.6511 33C8.2266 33 3 27.7734 3 21.3489C3 14.9247 8.2266 9.69809 14.6511 9.69809C21.0753 9.69809 26.3019 14.9247 26.3019 21.3489C26.3019 27.7734 21.0753 33 14.6511 33ZM14.6511 14.0556C10.6293 14.0556 7.3572 17.3274 7.3572 21.3492C7.3572 25.371 10.629 28.6431 14.6511 28.6431C18.6726 28.6431 21.945 25.3713 21.945 21.3492C21.9447 17.3274 18.6726 14.0556 14.6511 14.0556Z"
						  fill="#50C8EF"></path>
						<path d="M33.0027 14.6511H28.6455V7.3572H21.3516V3H33.0027V14.6511Z" fill="#50C8EF"></path>
						<path d="M29.2812 3.63574L32.3618 6.71628L22.8909 16.1872L19.8103 13.1066L29.2812 3.63574Z"
						  fill="#50C8EF"></path>
					  </svg>
					</div>
				  </div>
				  <div class="w-9 h-9 relative flex-col justify-start items-start flex" v-if='item.title === "Female"'>
					<svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36" fill="none">
					  <path
						d="M29.6016 11.6016C29.6016 5.19427 24.4073 0 18 0C11.5927 0 6.39844 5.19427 6.39844 11.6016C6.39844 17.2875 10.4919 22.0091 15.8906 23.0023V26.4375H10.6172V30.6562H15.8906V36H18H20.1094V30.6562H25.3828V26.4375H20.1094V23.0023C25.5081 22.0091 29.6016 17.2875 29.6016 11.6016ZM18 18.9844C13.9295 18.9844 10.6172 15.672 10.6172 11.6016C10.6172 7.5311 13.9295 4.21875 18 4.21875C22.0705 4.21875 25.3828 7.5311 25.3828 11.6016C25.3828 15.672 22.0705 18.9844 18 18.9844Z"
						fill="#FF8FB8"></path>
					  <path
						d="M20.1094 30.6562H25.3828V26.4375H20.1094V23.0023C25.5081 22.0091 29.6016 17.2875 29.6016 11.6016C29.6016 5.19427 24.4073 0 18 0V4.21875C22.0705 4.21875 25.3828 7.5311 25.3828 11.6016C25.3828 15.672 22.0705 18.9844 18 18.9844V36H20.1094V30.6562Z"
						fill="#FF5F96"></path>
					</svg>
				  </div>
				  <div class="text-white text-sm font-semibold font-[&#39;Poppins&#39;] leading-tight">{{item.title}}</div>
				</button>
			  </div>

			  <button  @click='sureCli' type="submit" style="background-color: rgb(244 63 94/var(--tw-bg-opacity)); width: 100%;"
				class="w-[242px] px-4 py-3 bg-rose-500 rounded-[10px] justify-center items-center gap-2 inline-flex">
				<div class="text-white text-sm font-semibold font-[&#39;Poppins&#39;] leading-tight">Ok</div>
			  </button>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>`,
	// < button type = "button" data - value="female"
	// 			  class= "gender-btn gender-selected w-[116px] px-10 pt-4 pb-2.5 bg-neutral-800 rounded-[10px] border border-neutral-700 flex-col justify-end items-center gap-2 inline-flex" >
	// <div class="w-9 h-9 relative flex-col justify-start items-start flex">
	// 	<svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36" fill="none">
	// 		<path
	// 			d="M29.6016 11.6016C29.6016 5.19427 24.4073 0 18 0C11.5927 0 6.39844 5.19427 6.39844 11.6016C6.39844 17.2875 10.4919 22.0091 15.8906 23.0023V26.4375H10.6172V30.6562H15.8906V36H18H20.1094V30.6562H25.3828V26.4375H20.1094V23.0023C25.5081 22.0091 29.6016 17.2875 29.6016 11.6016ZM18 18.9844C13.9295 18.9844 10.6172 15.672 10.6172 11.6016C10.6172 7.5311 13.9295 4.21875 18 4.21875C22.0705 4.21875 25.3828 7.5311 25.3828 11.6016C25.3828 15.672 22.0705 18.9844 18 18.9844Z"
	// 			fill="#FF8FB8"></path>
	// 		<path
	// 			d="M20.1094 30.6562H25.3828V26.4375H20.1094V23.0023C25.5081 22.0091 29.6016 17.2875 29.6016 11.6016C29.6016 5.19427 24.4073 0 18 0V4.21875C22.0705 4.21875 25.3828 7.5311 25.3828 11.6016C25.3828 15.672 22.0705 18.9844 18 18.9844V36H20.1094V30.6562Z"
	// 			fill="#FF5F96"></path>
	// 	</svg>
	// </div>
	// <div class="text-white text-sm font-semibold font-[&#39;Poppins&#39;] leading-tight">Female</div>
	// 			</button >
	data: function () {
		return {
			showBox: true,
			sexIndex: 0,
			sexList: [
				{
					title: 'Male'
				}, {
					title: 'Female'
				}
			]
		}
	},
	methods: {
		sureCli() {
			console.log(this.sexList[this.sexIndex].title, 'this.sexList[this.sexIndex].title')
			this.$emit('sureSex', this.sexList[this.sexIndex].title)
			this.closeBox()
		},
		closeBox() {
			this.showBox = false
		}
	}
});


Vue.component('html-header', {
	template: `<div
	class="bg-main fixed w-full top-0 z-40 flex h-16 shrink-0 items-center border-b border-[#363636] bg-main px-4 shadow-sm sm:gap-x-2 sm:px-6 lg:px-8">
	<button type="button" class="-m-2.5 p-2.5 text-gray-700 hidden">
	  <span class="sr-only">Open sidebar</span>
	  <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
		aria-hidden="true">
		<path stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"></path>
	  </svg>
	</button>
	<a href="./explore.html">
	  <img src="./images/logo.png"
		class="md:h-[30px] sm:h-[25px]">
	</a>
	<!-- Separator -->
	<div class="h-6 w-px bg-gray-900/10 lg:hidden" aria-hidden="true"></div>
	<div class="flex flex-1 self-stretch  bg-main">
	  <form class="relative flex flex-1" action="./explore.html#" method="GET"></form>
	  <!-- v-if='assToken === ""' -->
	  <div class="gap-x-2 flex" v-if="!isLogin">
		<div class="flex items-center gap-x-4 lg:gap-x-6 ">
		  <a href="./sign_up.html"
			class="w-[90px] h-8 px-4 py-1.5 border-[#E75275] border text-[#E75275] rounded-lg justify-center items-center gap-2 inline-flex">
			<div class=" text-sm font-semibold leading-tight">Register</div>
		  </a>
		</div>
		<div class="flex items-center gap-x-4 lg:gap-x-6">
		  <a href="./sign_in.html"
			class="w-[90px] h-8 px-4 py-1.5 bg-[#E75275] rounded-lg justify-center items-center gap-2 inline-flex">
			<div class="text-white text-sm font-semibold leading-tight">Login</div>
		  </a>
		</div>
	  </div>
	  <div class="flex items-center gap-x-4 lg:gap-x-6" v-if="isHui">
		<div
		  class="w-fit relative" style="margin-right: 1.5rem">
		  <button type="button"
			class="flex px-4 py-1.5 rounded-lg justify-center items-center gap-2 text-white border border-white/[.15]"
			@click="showchongzhi = !showchongzhi"
			>
			<img class="h-5 w-5"
			  src="./images/1.png"
			  alt="token icon">
			<div class="flex gap-1">
			  <span class="hidden sm:flex">tokens </span>
			  <turbo-cable-stream-source channel="Turbo::StreamsChannel"
				signed-stream-name="IloybGtPaTh2WTJoaGRDMWphR0YwTDFWelpYSXZORFV6T0RrNTp0b2tlbl9iYWxhbmNlIg==--988f59c0480e462f6ffe4aa3e6456380186471dc9566e1f7263859c595c0ad2f"></turbo-cable-stream-source>
			  <span id="user-token-balance">
				{{userInfo.tokens}}
			  </span>

			</div>
			<img class="hidden sm:flex"
			  src="./js/add-aa143e8e5c686a00ad832e4376ab9d4f679d68ebb95e4ea58b2cec4feaa6b7d4.svg"
			  alt="add tokens icon">
			<img class="sm:hidden h-5 w-5"
			  src="./js/expand-cd76d60d9e4264d89102db00cc87d69cb1ecd6c96663e5cea3809a90dd47a9ab.svg"
			  alt="expand menu">
		  </button>
		  <div v-if="showchongzhi"
			class="bg-zinc-900 md:w-full w-fit border border-white/[.15] absolute right-0 z-10 mt-2.5 origin-top-right rounded-lg shadow-lg focus:outline-none">
			<div class="flex px-4 flex-col bg-zinc-900 rounded-lg">
			  
			  <div class="w-full pb-3 pt-2.5 bg-zinc-900 justify-between items-center gap-1 inline-flex">
				<div class="text-white text-[13px] font-medium ">Image cost</div>
				<div class="justify-start items-center gap-1 flex">
				  <div class="text-right text-white text-[13px] font-bold">{{userInfo.imageConsTokens}}</div>
				  <div class="w-4 h-4">
					<img
					  src="./images/1.png">
				  </div>
				</div>
			  </div>
			</div>
			<div class="w-full px-4 pb-3 bg-zinc-900 rounded-lg justify-center items-center inline-flex">
			  <a class="w-[150px] h-8 p-3 bg-[#E75275] rounded-[10px] justify-center items-center gap-2 inline-flex"
				href="./Recharge.html">
				<div class="text-white text-sm font-semibold">Buy more</div>
				<img class="w-5 h-5"
				  src="./js/add_white-d504ef86384a5fe4014170fa04483735c82f42d80301af36e1c50f979a2c2245.svg">
			  </a>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="flex items-center" v-if="isLogin">
		<!-- Separator -->
		<div class="lg:block lg:h-6 lg:w-px lg:bg-gray-900/10" aria-hidden="true"></div>
		<!-- Profile dropdown -->
		<div  @keydown.escape="open = false" @click.away="open = false"
		  class="w-fit relative">
		  <button type="button" class="-m-1.5 flex items-center p-1.5 " id="user-menu-button" @click='openSetting = !openSetting'
			aria-expanded="false" >
			<span class="sr-only">Open user menu</span>

			<img src="./images/sex0.png"
			  class="h-8 w-8 rounded-full">
			<span class="hidden lg:flex lg:items-center">
			  <span class="ml-4 text-sm font-semibold leading-6 text-white" aria-hidden="true" style='display: inline-clock;' @click=''>{{userInfo.nickName}}</span>
			  <svg class="ml-2 h-5 w-5 text-white" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
				<path fill-rule="evenodd"
				  d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
				  clip-rule="evenodd"></path>
			  </svg>
			</span>
		  </button>
		  <div v-if='openSetting' @keydown.arrow-up.prevent="" @keydown.arrow-down.prevent=""
			@keydown.tab="openSetting = false" @keydown.enter.prevent="openSetting = false"
			class="absolute right-0 z-10 mt-2.5 origin-top-right rounded-lg shadow-lg focus:outline-none">
			<a class="rounded-t-lg w-full h-11 px-4 py-2.5 bg-zinc-900 hover:bg-gray-700 justify-start items-center gap-2 inline-flex"
			  href="./setting.html">
			  <div class="w-5 h-5 relative"><img
				  src="./js/settings-087d65eb2d9a7508f04c379467f39735f971ac6299872f159da8e6c1d735a710.svg"></div>
			  <div class="text-white text-sm font-semibold leading-normal">Settings</div>
			</a>
			<a class="w-full h-11 px-4 py-2.5 bg-zinc-900 hover:bg-gray-700 justify-start items-center gap-2 inline-flex"
			  href="./subscriptions.html">
			  <div class="w-5 h-5 relative"><img
				  src="./js/premium-bef4c0d01b6b99b6f2764cf40b755bd97debc307331857ca26f97628c6a38eec.svg"></div>
			  <div class="text-white text-sm font-semibold leading-normal">Subscription</div>
			</a> 
			<div class="button_to" ><div @click='logoutCli'
				class="rounded-b-lg w-full h-11 px-4 py-2.5 bg-zinc-900 hover:bg-gray-700 justify-start items-center gap-2 inline-flex">
				<div class="w-5 h-5 relative"><img
					src="./js/logout-c5bab4efa12211db16618320220423a09cf3d7f5b562be32cdd5110df2a21c18.svg"></div>
				<div class="text-white text-sm font-semibold leading-normal">Logout</div>
			  </div></div>
		  </div>
		</div>
	  </div>
	</div>
  </div>`,
	data: function () {
		return {
			isLogin: false,
			isHui: false,
			openSetting: false,
			showchongzhi: false,
			assToken: '',
			userInfo: '',
			isDingyue: false
		}
	},
	mounted() {
		const token = localStorage.getItem('token')
		this.assToken = token
		instance.get("/member/memberDetail")
			.then((res) => {
				if (res.data.code == '200' && res.data.data) {
					// console.log("用户信息", res.data);
					if (res.data.data.expirationTime) {
						new Date(Number(res.data.data.expirationTime)) - new Date() > 0 ? res.data.data.expirationState = 1 : res.data.data.expirationState = 0
					} else {
						res.data.data.expirationState = 0
					}
					if (res.data.data.expirationState > 0) {
						this.isHui = true
					}
					localStorage.setItem('userInfo', JSON.stringify(res.data.data))
					this.userInfo = JSON.parse(localStorage.getItem('userInfo'))
					// console.log(res.data.data, 'res.data.datares.data.data')
				}

			})
			.catch((err) => {
				console.log(err);
			});
		if (!this.assToken) {
			this.isLogin = false
		} else {
			this.isLogin = true
		}
	},
	methods: {
		logoutCli() {
			localStorage.clear()
			window.location.href = './sign_in.html'
		},
	}
});


Vue.component('toast', {
	template: `<div data-create-flow-target="toast" class="bg-[#C23934] w-[280px] ml-2 z-40 rounded-lg top-[80px] lg:right-[82px] left-auto right-auto text-white flex gap-3 px-4 py-3 justify-center items-start absolute animate-slideFromRight duration-300 lg:mx-0">
	<img src="/assets/create-char/crossed_circle-4248764afe392ec6862750722104a2ca280f995d373b5c53165b91391c4d5ca3.svg">
	<div class="flex flex-col">
	  <p class="leading-[24px] font-medium text-[16px]">Incomplete Choices</p>
	  <p class="font-normal leading-[20px] text-[13px]">Please finish your selection</p>
	</div>
	<button data-action="click->create-flow#closeToast" class="w-6 h-6">
	  <img src="/assets/create-char/close_error-d4ec52d2c48c0b4ccde2f4e9b204ce06649bbbaa185dcfc4802a1cdf721ea2ba.svg">
	</button>
  </div>`,
	data: function () {
		return {
			isLogin: true,
			isHui: true,
			openSetting: false,
			showchongzhi: false
		}
	},
	mounted() {
		console.log(this.isLogin, this.isHui)
	},
	methods: {
		logoutCli() {
			localStorage.setItem('token', {})
			window.location.reload()
		},
	}
});


Vue.component('shiba', {
	template: `<div id="staticModal" data-modal-backdrop="static" tabindex="-1" aria-hidden="true" class="fixed top-0 left-0 right-0 z-50 w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full flex justify-center items-center">
    <div class="relative max-h-full md:max-w-[474px] w-full">
      <!-- Modal content -->
      <div class="relative rounded-lg shadow dark:bg-gray-700">
        <div class="h-[506px] md:p-10 p-2 bg-stone-900 rounded-[10px] flex-col justify-center items-center inline-flex">
          <div class="self-stretch flex-col justify-start items-start gap-6 inline-flex">
            <div class="flex-col justify-start items-center gap-3.5 flex">
              <div class="text-[#E75275] text-[32px] font-bold leading-[42px]">Warning 18+</div>
              <div class="text-center text-white text-base font-semibold leading-relaxed">This site is for adults only!<br>
                It contains AI-generated adult content.</div>
              <div class="text-center text-white text-sm font-light leading-normal">By entering this website, you confirm that you are 18 years old or more. By using the site, you agree to our Terms of Service. Our privacy policy details how we collect and use your data. We use cookies for basic analytics and spam detection.All content on this website are AI-generated! Any generations that resemble real people are purely coincidental.</div>
            </div>
            <div class="m-auto justify-center items-center gap-3 flex md:text-base text-xs">
              <a target="_blank" href="https://find-heart.cn-sh2.ufileos.com/sugar/service/Terms%20of%20Service.pdf " class="md:px-4 md:py-3 px-1 py-1 rounded-[10px] border border-[#E75275] justify-center items-center gap-2 flex">
                <div class="text-[#E75275] md:text-sm text-xs font-semibold leading-tight">Terms of Service</div>
              </a>
              <a href="" class="md:px-3 md:py-3 px-1 py-1 rounded-[10px] border border-[#E75275] justify-center items-center gap-2 flex">
                <div class="text-[#E75275] md:text-sm text-xs font-semibold leading-tight">Privacy Policy</div>
              </a>
              <div class="md:px-4 md:py-3 px-1 py-1 rounded-[10px] border border-[#E75275] justify-center items-center gap-2 flex">
                <a href="" class="text-[#E75275] md:text-sm text-xs font-semibold leading-tight">Cookies</a>
              </div>
            </div>
            <input value="true" autocomplete="off" type="hidden" name="user[agreed_status]" id="user_agreed_status">
            <button class="w-full px-4 py-3 bg-[#E75275] rounded-[10px] border border-[#E75275] justify-center items-center gap-2 inline-flex">
              <div class="text-white text-sm font-semibold leading-tight">I am over 18 - Continue</div>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>`,
	data: function () {
		return {
			isLogin: true,
			isHui: true,
			openSetting: false,
			showchongzhi: false
		}
	},
	mounted() {
		console.log(this.isLogin, this.isHui)
	},
	methods: {
		logoutCli() {
			localStorage.setItem('token', {})
			window.location.reload()
		},
	}
});
Vue.component('createloading', {
	template: ` <div id="image-loader-container" data-controller="percentage-counter"
    class="flex justify-center items-center h-screen mx-auto md:w-[760px] w-fit mx-10 mt-6 md:mb-6 mb-[72px] max-w-2xl lg:max-w-none relative bg-zinc-900 rounded-lg border border-zinc-800">
    <div class="mx-12">
      <div class="w-24 h-24 m-auto relative" style="border: 8px solid #A06ED4;
	  border-radius: 50%;
	  display: flex;
	  align-items: center;">
        <svg class="image-loader" xmlns="http://www.w3.org/2000/svg" width="98" height="98" viewBox="0 0 98 98"
          fill="none">
          <path
            d="M3.675 49C1.64535 49 -0.0143422 50.6477 0.137726 52.6716C0.768717 61.0698 3.55614 69.1861 8.25799 76.2229C13.6422 84.2809 21.2949 90.5614 30.2485 94.2701C39.2021 97.9788 49.0544 98.9492 58.5594 97.0585C68.0645 95.1678 76.7955 90.501 83.6482 83.6482C90.501 76.7955 95.1678 68.0645 97.0585 58.5594C98.9492 49.0544 97.9788 39.2021 94.2701 30.2485C90.5614 21.2949 84.281 13.6422 76.2229 8.25799C69.1861 3.55615 61.0698 0.768719 52.6716 0.137727C50.6477 -0.0143419 49 1.64535 49 3.675C49 5.70465 50.6486 7.33316 52.6704 7.51201C59.613 8.12617 66.3118 10.4754 72.1395 14.3693C78.9888 18.9459 84.3272 25.4507 87.4796 33.0612C90.632 40.6718 91.4568 49.0462 89.8497 57.1255C88.2426 65.2048 84.2759 72.6261 78.451 78.451C72.6261 84.2759 65.2048 88.2426 57.1255 89.8497C49.0462 91.4568 40.6718 90.632 33.0612 87.4796C25.4507 84.3272 18.9458 78.9888 14.3693 72.1395C10.4754 66.3118 8.12617 59.613 7.51201 52.6703C7.33316 50.6486 5.70465 49 3.675 49Z"
            fill="url(#paint0_angular_4678_15737)"></path>
          <defs>
            <radialgradient id="paint0_angular_4678_15737" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse"
              gradientTransform="translate(49 49) rotate(164.055) scale(44.5907 41.6811)">
              <stop stop-color="#E75275"></stop>
              <stop offset="0.302083" stop-color="#E75275"></stop>
            </radialgradient>
          </defs>
        </svg>
        <div data-percentage-counter-target="display" style="line-height: inherit;"
          class="text-center left-0 right-0 top-[25px] text-rose-400 text-md font-semibold leading-loose m-auto absolute justify-center items-center">
          {{minuTime}}%</div>
      </div>
      <div class="flex-col justify-center items-center gap-2 flex mt-5">
        <div class="text-white text-base font-semibold leading-relaxed">Please wait!</div>
        <div class="text-neutral-200 text-sm font-medium leading-tight"> This might take a few minutes</div>
      </div>
    </div>
  </div>`,
	data: function () {
		return {
			isLogin: true,
			isHui: true,
			openSetting: false,
			showchongzhi: false,
			minuTime: 0
		}
	},
	mounted() {
		this.minuTime = 0;
		let that = this
		var timer = setInterval(function () {
			// for(i=0;i<=100;i++){
			// i+=1;
			console.log(that.minuTime, 'minuTime')
			if (that.minuTime < 100) {
				that.minuTime += Math.floor(Math.random() * 10);
				// load.style.width = i * 3 + 'px';
				// result.innerText = i + '%';
				console.log(that.minuTime + '%', "i + '%'")
			}
			if (that.minuTime >= 100) {
				that.minuTime = 100
				clearInterval(timer);
			}
			console.log(that.minuTime)
			// }
		}, 1000);
	},
	methods: {
		logoutCli() {
			localStorage.setItem('token', {})
			window.location.reload()
		},
	}
});

Vue.component('sleectname', {
	template: ` <div>
	<div class="fixed top-0 left-0 right-0 bottom-0 z-50 bg-black bg-opacity-50 backdrop-blur-sm" style="margin-top: 0px;"></div>
	<div id="staticModal" data-modal-backdrop="static" tabindex="-1" aria-hidden="true" class="fixed top-0 left-0 right-0 z-50 w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full flex justify-center items-center">
	  <div class="relative max-h-full">
		<!-- Modal content -->
		<div class="relative rounded-lg shadow dark:bg-gray-700">
		  <div class="w-[335px] h-[354px] bg-stone-900 rounded-[10px] border border-neutral-700">
			<div class="w-[146px] h-[222px] left-[-2px] top-[112px] absolute origin-top-left -rotate-90 opacity-60 bg-fuchsia-500 rounded-full blur-[300px]"></div>
			<div class="w-[146px] h-[222px] left-[208px] top-[452px] absolute origin-top-left -rotate-90 bg-indigo-500 rounded-full blur-[300px]"></div>
			<div class="w-5 h-5 left-[303px] top-[12px] absolute"></div>
			<div class="left-[28px] top-[28px] absolute flex-col justify-start items-start gap-6 inline-flex">
			  <div class="flex-col justify-start items-start gap-2 flex">
				<div class="text-white text-lg font-bold leading-7">Personalize Your AI Experience
				</div>
				<div class="w-[279px] text-neutral-400 text-sm font-medium leading-tight">It will help to customize your interactions with the AI characters.</div>
			  </div>
			  <div class="flex-col justify-start items-start gap-7 flex">
				<div class="flex relative">
				  <div class="absolute w-5 h-5 mt-[12px] ml-[10px] pl-[3px] pr-[2.68px] py-0.5 flex-col justify-center items-center gap-[0.30px]">
					<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
					  <path d="M10.1542 2C7.86847 2 6 3.86847 6 6.1542C6 8.43993 7.86847 10.3084 10.1542 10.3084C12.4399 10.3084 14.3084 8.43993 14.3084 6.1542C14.3084 3.86847 12.4399 2 10.1542 2Z" fill="#8D8D8D"></path>
					  <path d="M17.2913 13.628C17.1825 13.3559 17.0374 13.1019 16.8741 12.8661C16.0396 11.6325 14.7517 10.8162 13.3004 10.6167C13.119 10.5985 12.9195 10.6348 12.7743 10.7436C12.0124 11.306 11.1054 11.5962 10.1621 11.5962C9.21876 11.5962 8.31175 11.306 7.54984 10.7436C7.4047 10.6348 7.20515 10.5804 7.02376 10.6167C5.57251 10.8162 4.2664 11.6325 3.45008 12.8661C3.28682 13.1019 3.14168 13.3741 3.03286 13.628C2.97845 13.7369 2.99657 13.8639 3.05098 13.9727C3.19612 14.2267 3.37751 14.4807 3.54077 14.6983C3.79473 15.043 4.06685 15.3514 4.37525 15.6416C4.62921 15.8956 4.91945 16.1314 5.20973 16.3673C6.64282 17.4376 8.36619 17.9999 10.144 17.9999C11.9217 17.9999 13.6451 17.4375 15.0782 16.3673C15.3684 16.1496 15.6587 15.8956 15.9127 15.6416C16.2029 15.3514 16.4932 15.043 16.7472 14.6983C16.9286 14.4625 17.0919 14.2267 17.237 13.9727C17.3276 13.8639 17.3457 13.7368 17.2913 13.628Z" fill="#8D8D8D"></path>
					</svg>
				  </div>
				  <input label="false" placeholder="Nickname" required="required" class="w-[279px] pl-[40px] pr-[15px] py-3 bg-neutral-800 rounded-[10px] border border-neutral-700 justify-start items-center gap-2 inline-flex grow shrink basis-0 text-neutral-200 text-sm font-medium leading-[22px]" type="text" name="user[nickname]" id="user_nickname" data-gtm-form-interact-field-id="0">
				</div>
				<select class="w-[279px] px-[15px] py-3 bg-neutral-800 rounded-[10px] border border-neutral-700 justify-start items-center gap-2 inline-flex grow shrink basis-0 text-neutral-200 text-sm font-medium leading-[22px]" required="required" name="user[gender]" id="user_gender"><option selected="selected" value="male">Male</option>
  <option value="female">Female</option></select>
				<button name="button" type="submit" class="w-[279px] px-4 py-3 bg-[#E75275] rounded-[10px] border border-[#E75275] justify-center items-center gap-2 inline-flex" data-controller="loading-button" data-loading-button-target="submit">
				  <div class="w-5 h-5 relative">
					<svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
					  <path d="M17.7186 4.01196L16.488 2.78137C15.988 2.28064 15.3101 2 14.6027 2H4.33337C3.32068 2 2.5 2.82068 2.5 3.83337V16.1666C2.5 17.1793 3.32068 18 4.33337 18H16.6666C17.6793 18 18.5 17.1793 18.5 16.1666V5.89734C18.5 5.18994 18.2194 4.51196 17.7186 4.01196ZM5.16663 6.16663V4.5C5.16663 4.224 5.39062 4 5.66663 4H12.6666C12.9426 4 13.1666 4.224 13.1666 4.5V6.16663C13.1666 6.44263 12.9426 6.66663 12.6666 6.66663H5.66663C5.39062 6.66663 5.16663 6.44263 5.16663 6.16663ZM10.5 15.3334C8.84338 15.3334 7.5 13.99 7.5 12.3334C7.5 10.6766 8.84338 9.33337 10.5 9.33337C12.1566 9.33337 13.5 10.6766 13.5 12.3334C13.5 13.99 12.1566 15.3334 10.5 15.3334Z" fill="white"></path>
					</svg>
				  </div>
				  <div class="text-white text-sm font-semibold leading-tight">Save</div>
  </button>            </div>
			</div>
		  </div>
		  <!--
		  <div class="w-[335px] h-[380px] relative bg-stone-900 rounded-[10px]">
			<div class="w-[146px] h-[222px] left-[-2px] top-[112px] absolute origin-top-left -rotate-90 opacity-60 bg-fuchsia-500 rounded-full blur-[300px]"></div>
			<div class="w-[146px] h-[222px] left-[208px] top-[452px] absolute origin-top-left -rotate-90 bg-indigo-500 rounded-full blur-[300px]"></div>
			<div class="left-[24px] top-[24px] absolute flex-col justify-start items-start gap-5 inline-flex">
			  <div class="flex-col justify-start items-start gap-3.5 flex">
				<div class="text-white text-lg font-bold leading-7">Welcome on...</div>
				<div class="text-neutral-400 text-sm font-medium leading-tight">Please enter your nickname.</div>
			  </div>
			  <div class="form-inputs">
				<div class="mt-2">
				  <div class="w-[287px] h-[72px] flex-col justify-start items-start gap-1.5 inline-flex">
					<div class="self-stretch h-[72px] flex-col justify-start items-start gap-1.5 flex relative">
					  <div class="self-stretch text-neutral-500 text-sm font-medium leading-snug">Nickname</div>
					  <div class="w-5 h-5 absolute top-[38px] left-[10px]">
						<img src="/assets/user-136475e1e7120a61ab13061e84c88cdf96837d2fb8eafda7a8c5b73815512f42.svg">
					  </div>
					  <input label="false" placeholder="Enter Nickname" required="required" class="self-stretch pl-10 px-3.5 py-2.5 bg-neutral-800 rounded-lg shadow text-neutral-200 text-sm font-medium leading-[14px] justify-start items-center gap-2 inline-flex" type="text" name="user[nickname]" id="user_nickname" />
					</div>
				  </div>
				</div>
				<div class="mt-2">
				  <div class="w-[287px] h-[72px] flex-col justify-start items-start gap-1.5 inline-flex">
					<div class="self-stretch h-[72px] flex-col justify-start items-start gap-1.5 flex">
					  <div class="self-stretch text-neutral-500 text-sm font-medium leading-snug">Gender</div>
						<div class="w-5 h-5 relative"></div>
						<select class="self-stretch px-3.5 py-2.5 bg-white rounded-lg shadow border border-gray-300 justify-start items-center gap-2 inline-flex" name="user[gender]" id="user_gender"><option value="male">Male</option>
  <option value="female">Female</option></select>
					</div>
				  </div>
				</div>
			  </div>
			  <button name="button" type="submit" class="w-[287px] text-white text-sm font-semibold leading-tight px-4 py-2.5 bg-cyan-700 rounded-[10px] justify-center items-center gap-2 inline-flex" data-controller="loading-button" data-loading-button-target="submit">
				   Confirm
  </button>          </div>
		  </div>
		  -->
		</div>
	  </div>
	</div>
	</div>`,
	data: function () {
		return {
			isLogin: true,
			isHui: true,
			openSetting: false,
			showchongzhi: false
		}
	},
	mounted() {
		console.log(this.isLogin, this.isHui)
	},
	methods: {
		logoutCli() {
			localStorage.setItem('token', {})
			window.location.reload()
		},
	}
});


Vue.component('contact', {
	template: ` <div>
	<div
	class="fixed top-0 left-0 right-0 bottom-0 z-50 bg-black bg-opacity-50 backdrop-blur-sm  email-modal modal-backdrop feedback-modal"
	data-modal="feedback-modal" style="margin-top: 0px;"></div>
  <div id="staticModal" data-modal-backdrop="static" tabindex="-1"
	class="fixed top-0 left-0 right-0 z-50 w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full flex justify-center items-center  feedback-modal">
	<div class="relative max-h-full">
	  <div class="relative rounded-lg shadow dark:bg-gray-700">
		<div class="w-[356px] h-[210px] relative bg-stone-900 rounded-[10px] border border-neutral-700">
		  <div class="w-[396px] h-[354px] relative bg-stone-900 rounded-[10px] border border-neutral-700">
			<div
			  class="w-[146px] h-[222px] left-[-2px] top-[112px] absolute origin-top-left -rotate-90 opacity-60 bg-fuchsia-500 rounded-full blur-[300px]">
			</div>
			<div
			  class="w-[146px] h-[222px] left-[208px] top-[452px] absolute origin-top-left -rotate-90 bg-indigo-500 rounded-full blur-[300px]">
			</div>
			<div class="w-5 h-5 left-[364px] top-[12px] absolute"></div>
			<div class="left-[28px] top-[28px] absolute flex-col justify-start items-start gap-6 inline-flex">
			  <div class="flex-col justify-start items-start gap-2 flex">
				<div class="text-white text-2xl font-bold leading-[34px]">Contact us
				  <img src="./images/close.png" @click="reload"
					class="absolute top-[7px] right-[10px] cursor-pointer feedback-modal-close">
				</div>
				<div class="w-[340px] text-neutral-400 text-sm font-medium leading-tight">Write your message here.
				</div>
			  </div>
			  <div class="flex-col justify-start items-start gap-7 flex">
				<div>
				  <textarea label="false"
					class="w-[340px] h-[120px] p-[15px] text-white bg-neutral-800 rounded-[10px] border border-neutral-700 justify-start items-center gap-2.5 inline-flex"
					placeholder="Description" v-model='contactText' id="feedback_description"></textarea>
				</div>
				<input value="482731" autocomplete="off" type="hidden" name="feedback[user_id]"
				  id="feedback_user_id">
				<input autocomplete="off" type="hidden" name="feedback[conversation_id]"
				  id="feedback_conversation_id">
				<input autocomplete="off" type="hidden" name="feedback[message_id]" id="feedback_message_id">
				<button
				  class="w-[340px] px-4 py-3 bg-[#E75275] rounded-[10px] border border-[#E75275] justify-center items-center gap-2 inline-flex">
				  <div class="text-white text-sm  leading-tight" @click='sendText'>Send</div>
				</button>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>
	</div>`,
	data: function () {
		return {
			contactText: ''
		}
	},
	mounted() {
		this.contactText = ''
	},
	methods: {
		reload() {
			location.reload()
		},


		sendText() {
			console.log(this.contactText, 'contactTextcontactText')
			if (this.contactText) {
				instance.post("/feedback/v1/escalation", { description: this.contactText })
					.then((result) => {
						// result.data才是真正的返回结果
						console.log("数据：", result.data);
						if (result.data.code === '200') {
							location.reload()
						}
						// this.indexList = result.data.data.list
					})
					.catch((err) => {
						console.log(err);
					});
			}
		}
	}
});
