
// const instance = axios.create({
//     baseURL: '',
//     timeout: 10000,
//     headers: {
//         'Content-Type': 'application/json;charset=UTF-8'
//     }
// })
// // 添加请求拦截器
// instance.interceptors.request.use(config =>{
// // 添加Token到请求头中
// const token = localStorage.getItem('token')
// console.log(token, 'tokentoken')
// if (token) {
// config.headers.Authorization = `Bearer ${token}`
// }
// return config
// }, error =>{
// return Promise.reject(error)
// })
// // 添加响应拦截器
// instance.interceptors.response.use(response =>{
// return response
// }, error =>{
// // 处理错误
// return Promise.reject(error)
// })
// // instance.interceptors.request.use(
// //   config => {
// //     // config.headers["Authorization"] = token;
// //     if (store.getters.token) {
// //       // let each request carry token
// //       // ['X-Token'] is a custom headers key
// //       // please modify it according to the actual situation
// //       config.headers["token"] = getToken();
// //     }
// //     return config;
// //   },
// //   error => {
// //     // do something with request error
// //     console.log(error); // for debug
// //     return Promise.reject(error);
// //   }
// // );

// function get(url, params) {
//    return new Promise((resolve, reject) => {
//     instance.get(url, { params: params })
//         .then((res) => {
//             resolve(res.data);
//         })
//         .catch((err) => {
//             reject(err.data);
//         });
//   });
// }
// function post(url, data) {
//   return new Promise((resolve, reject) => {
//     instance.post(url, data,{
//             headers:{
//                 'Content-Type':'application/json'
//             }
//         })
//         .then((res) => {
//             resolve(res.data);
//         })
//         .catch((err) => {
//             reject(err);
//         });
//     });
// }

//axios封装post请求
// function axiosPostRequst(url,data) {
//     const token = localStorage.getItem('token')
//     console.log(token, 'token')
//     let result = axios({
//         method: 'get',
//         url: url,
//         data: data,
//         headers:{
//             'Content-Type':'application/x-www-form-urlencoded',
//             // 'token':'application/x-www-form-urlencoded'
//         }
//     }).then(resp=> {
//         return resp.data;
//     }).catch(error=>{
//         return "exception="+error;
//     });
//     return result;
// }
// (() => {
//   console.log(123123)
//   document.write("<script src='./ common / vue.min.js'></script >");
// })()


const instance = axios.create({
  // baseURL: 'http://tzzds3nw.dongtaiyuming.net',
  baseURL: 'https://api.dreamyai.vip',
  // baseURL: 'https://renewed-kingfish-safely.ngrok-free.app',
  // baseURL: 'https://dreamyai.vip/dreamy',
  // baseURL: 'https://22n33a9304.imdo.co',
  timeout: 900000,
  headers: {
    'Content-Type': 'application/json;charset=UTF-8'
  }
})
// 设置请求拦截器
instance.interceptors.request.use(
  // config就是本次发请求的信息
  config => {
    // 在发送请求之前做些什么
    const token = localStorage.getItem('token')
    // config.headers["Authorization"] = token;
    if (token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers["Authorization"] = token;
    }
    // console.log('请求拦截器', config);
    return config;
  },
  error => {
    // 对请求错误做些什么
    // console.error(error);
    return Promise.reject(error);
  }
);

// 设置响应拦截器
instance.interceptors.response.use(
  response => {
    // 对响应数据做些什么
    // console.log('响应拦截器', response);
    // if (response.data.code === '401') {
    //   alert(response.data.msg || response.data.message || response.data.data)
    // }
    console.log(response.data.code, 'response.data.code', window.location.pathname)
    if (response.data.code === '200' || response.data.code === '20002' || response.data.code === '20003' || ((window.location.pathname.includes('explore.html') || window.location.pathname === '/') && response.data.code == '401')) {
      return response;
    } else if (response.data.code === '4001' || (window.location.pathname.includes('chart.html') && response.data.code == '401')) {
      // location.href = '../sign_in.html'
      window.location.replace("./sign_in.html");
      return
    } else {
      if (response.data.code !== '401' && response.data.code) {
        alert(response.data.info || response.data.msg || response.data.message || response.data.data)
      }

    }
  },
  error => {
    // 对响应错误做些什么
    console.error('报错', error);
    return Promise.reject(error);
  }
);